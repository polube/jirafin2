export function getProjects() {
    return new Promise((resolve) => {
        window.AP.request('/rest/api/3/project', {
            success: function (response, req) {
                resolve(JSON.parse(response));
            },
        });
    });
}

export function getUsers(projectName) {
    return new Promise((resolve) => {
        window.AP.request(
            `/rest/api/2/user/assignable/search?project=${projectName}`,
            {
                success: function (response, req) {
                    resolve(JSON.parse(response));
                },
            }
        );
    });
}

export function getIssues(projectName) {
    return new Promise((resolve) => {
        window.AP.request(
            `/rest/api/3/search?jql=project%20%3D%20${projectName}`,
            {
                success: function (response, req) {
                    resolve(JSON.parse(response));
                },
            }
        );
    });
}

export function getFields() {
    return new Promise((resolve) => {
        window.AP.request('/rest/api/3/field', {
            success: function (response, req) {
                resolve(JSON.parse(response));
            },
        });
    });
}
