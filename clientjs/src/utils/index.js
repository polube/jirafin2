import { FIELD_NAMES } from '../constans';

export function sortFields(fields) {
    return [...fields].sort(
        (ob1, ob2) =>
            FIELD_NAMES.indexOf(
                ob1.clauseNames.find((name) =>
                    FIELD_NAMES.some((val) => val === name)
                )
            ) -
            FIELD_NAMES.indexOf(
                ob2.clauseNames.find((name) =>
                    FIELD_NAMES.some((val) => val === name)
                )
            )
    );
}

export function filterFields(fields) {
    return [...fields].filter((field) => {
        for (let i of Object.values(field.clauseNames)) {
            if (FIELD_NAMES.some((name) => name === i)) {
                return field;
            }
        }
    });
}

export function createRaciStrng(issue, user, fields) {
    debugger;
    return fields.reduce((RACIString, field) => {
        const fieldName = field.clauseNames.find((name) =>
            FIELD_NAMES.some((val) => val === name)
        );

        if (
            issue.fields[field.id] &&
            user.accountId === issue.fields[field.id][0].accountId
        ) {
            RACIString += fieldName.substr(0, 1);
        }
        return RACIString;
    }, '');
}
