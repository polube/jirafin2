import React, { useState, useEffect } from 'react';
import Selecter from './components/Selecter';
import './index.css';
import { getUsers, getIssues, getFields, getProjects } from './api/index';
import { sortFields, filterFields, createRaciStrng } from './utils';

function App() {
    const [allProjects, setProjects] = useState();
    const [allIssues, setIssues] = useState();
    const [allUsers, setUsers] = useState();
    const [allFields, setFields] = useState();
    const [projectName, setProjectName] = useState();
    const [electProject, setElectProject] = useState();

    useEffect(() => {
        getProjects().then((proj) => {
            if (!projectName) {
                setProjectName(proj[0].key);
            }

            setProjects(proj);
        });
    }, []);

    useEffect(() => {
        Promise.all([
            getFields(),
            getUsers(projectName),
            getIssues(projectName),
        ]).then(([fields, users, issues]) => {
            setFields(fields);
            setUsers(users);
            setIssues(issues);
        });
    }, [projectName]);

    useEffect(() => {
        if (!allFields) return;

        const fields = filterFields(allFields);

        setElectProject(sortFields(fields));
    }, [allFields]);

    if (allIssues) {
        return (
            <>
                <Selecter projects={allProjects} set={setProjectName} />

                <table className="table">
                    <thead>
                        <tr>
                            <th key={0} className="nope"></th>

                            {allUsers.map((user) => {
                                return (
                                    <th key={user.accountId}>
                                        {user.displayName}
                                    </th>
                                );
                            })}
                        </tr>
                    </thead>

                    <tbody>
                        {allIssues.issues.map((issue) => {
                            return (
                                <tr key={issue.key}>
                                    <td
                                        key={issue.key + '_1'}
                                        className="firstTd"
                                    >
                                        {issue.fields.summary}
                                    </td>

                                    {allUsers.map((user) => {
                                        return (
                                            <td
                                                key={
                                                    user.accountId +
                                                    '_2' +
                                                    issue.key
                                                }
                                                className="field"
                                            >
                                                {createRaciStrng(
                                                    issue,
                                                    user,
                                                    electProject
                                                )}
                                            </td>
                                        );
                                    })}
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </>
        );
    } else {
        return (
            <>
                <p>DOWNLOADING!</p>
            </>
        );
    }
}

export default App;
