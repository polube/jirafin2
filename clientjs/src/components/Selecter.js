import React from 'react';
import Select from '@atlaskit/select';

const Selecter = (props) => (
    <>
        <Select
            inputId="single-select-example"
            className="single-select"
            classNamePrefix="react-select"
            options={props.projects.map((project) => {
                return { label: project.name, value: project.key };
            })}
            placeholder="Pick a project"
            onChange={(e) => {
                props.set(e.value);
            }}
        />
    </>
);

export default Selecter;
