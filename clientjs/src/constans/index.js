export const FIELD_NAMES = [
    'Responsible[People]',
    'Accountable[People]',
    'Consulted[People]',
    'Informed[People]',
];
